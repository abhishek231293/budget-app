class Jokes {
    constructor(){
        this.baseUrl = 'https://api.chucknorris.io/jokes';
    }    

    async getcategoryList(){

        const response = await fetch(this.baseUrl+'/categories')

        if(response.status === 200){
            return await response.json()
        }else{
            return []
        }
    }

    async getRandomJokes(category){
        let categoryCond = '';

        if(category){
            categoryCond = `?category=${category}`
        }
        
        const response = await fetch(this.baseUrl+'/random'+categoryCond)

        if(response.status === 200){
            const data = await response.json()
            return data.value

        }else{
            return []
        }
    }
}

module.exports = Jokes