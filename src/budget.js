const uuid4 = require('uuid4');

class Budget {

    constructor(){
        const data = localStorage.getItem('budget-app')

        if(data){
            this.budgetDetail = JSON.parse(data)
        }else{
            this.budgetDetail = {"budget":0, "expenses":[]}
        }

    }

    addNewBudget(amount){
        this.budgetDetail = {"budget":amount, "expenses":[]}
        this.storeData()
    }

    addExpense(title, amount){
        this.budgetDetail.expenses.push(
            {
                "id": uuid4(),
                "expense": title,
                "amount": amount
            }
        )

        this.storeData()
    }

    getBudgetAmount(){
        return this.budgetDetail.budget
    }

    getExpenseAmount(){
        let totalExpense = 0
        this.budgetDetail.expenses.forEach((expense)=>{
            totalExpense += expense.amount
        })

        return totalExpense
    }

    deleteExpense(expenseId){

        this.budgetDetail.expenses = this.budgetDetail.expenses.filter((expense)=>expense.id != expenseId)

        this.storeData()

    }

    editExpense(expenseId, title, amount){

        this.budgetDetail.expenses.forEach((expense)=>{
            if(expense.id === expenseId){
                expense.expense = title
                expense.amount = amount
            }
        })
        
        this.storeData()

    }

    getExpenseList(){
        return this.budgetDetail.expenses.reverse()
    }

    getExpense(id){
        return this.budgetDetail.expenses.find((expense)=>expense.id === id)
    }

    getBalanceAmount(){
        return this.budgetDetail.budget - this.getExpenseAmount()
    }

    storeData(){
        localStorage.setItem('budget-app', JSON.stringify(this.budgetDetail))
    }
}

module.exports = Budget