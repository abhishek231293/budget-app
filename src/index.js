import Budget from './budget'
import View from './view'
import Jokes from './jokes'
import Swal from 'sweetalert2'

const myBudget = new Budget();
const view = new View();
const jokes = new Jokes();



const getCategoryList = () => {
    jokes.getcategoryList().then((categories)=>{
        let options = '<option> Select Category </option>';
        categories.forEach((category)=>{
            options += `<option value="${category}"> ${category.toUpperCase()} </option>`
        })

        const categoryQuery = document.querySelector('#categories');
        categoryQuery.innerHTML = options
    }).catch((error)=>{
        console.log(error);
    })
}

const getRandomJokes = () =>{

    const category = document.getElementById('categories').value
    
    const jokesQuery = document.querySelector('#jokes');
    jokesQuery.innerHTML =  'Loading new Joke ...'

    jokes.getRandomJokes(category).then((result)=>{
        jokesQuery.innerHTML = result
    }).catch((error)=>{
        console.log(error);
    })
}



const getAmounts = () => {
    const budget = myBudget.getBudgetAmount();
    const expense = myBudget.getExpenseAmount();
    const balance = myBudget.getBalanceAmount();
    return {budget, balance, expense}
}

const resetExpenseForm = () => {
    const expenseQuery = document.querySelector('#new-expense')
    const expenseAmountQuery = document.querySelector('#new-expense-amount')
    expenseQuery.value = ''
    expenseAmountQuery.value = ''

    const updateButtonQuery = document.querySelector('#updateExpenseButton')
    const adddButtonQuery = document.querySelector('#addExpenseButton')
    updateButtonQuery.style.display = 'none';
    adddButtonQuery.style.display = '';
}

const resetBudgetForm = () => {
    const amountQuery = document.querySelector('#new-budget-amount')
    amountQuery.value = ''
}

const deleteExpense = (id) =>{
    myBudget.deleteExpense(id);
    Swal.fire('Success', 'Expense deleted successfully!', 'success')
    renderCardAmount()
    renderExpenseList()
}

const updateExpense = (id, oldExpense) => {
    const expenseQuery = document.querySelector('#new-expense');
    const amountQuery = document.querySelector('#new-expense-amount');
    const expense = expenseQuery.value;
    const amount = amountQuery.value;

    if(!expense.trim()){
        Swal.fire('Error', 'Expense is required!', 'error')        
        return
    }else if(!amount.trim()){
        Swal.fire('Error', 'Amount is required!', 'error')        
        return
    }

    const {balance} = getAmounts();

    const updatedbalance = balance + oldExpense;

    if(updatedbalance == 0){
        Swal.fire('Error', 'You cant add more expense!', 'error')        
        return
    }else if(updatedbalance< amount){
        Swal.fire('Error', 'Your balance is low add lower amount expense!', 'error')        
        return
    }

    myBudget.editExpense(id, expense, parseInt(amount));
    renderCardAmount()
    renderExpenseList()
    resetExpenseForm()
    $('#addExpense').modal('hide')
    Swal.fire('Success', 'Expense added successfully!', 'success') 
}

const editExpenseModel = (id) =>{
    
    const expense = myBudget.getExpense(id);
    $('#addExpense').modal('show')
    
    const expenseQuery = document.querySelector('#new-expense')
    const expenseAmountQuery = document.querySelector('#new-expense-amount')
    expenseQuery.value = expense.expense
    expenseAmountQuery.value = expense.amount
    const updateButtonQuery = document.querySelector('#updateExpenseButton')
    const adddButtonQuery = document.querySelector('#addExpenseButton')

    updateButtonQuery.style.display = '';
    adddButtonQuery.style.display = 'none';

    const updateExpenseButtonQuery = document.querySelector('#updateExpenseButton');

    updateExpenseButtonQuery.addEventListener('click', function(){
        updateExpense(id, expense.amount)
    })

    
}

const addBudgetButtonQuery = document.querySelector('#add-budget-button');
addBudgetButtonQuery.addEventListener('click', ()=>{
    resetBudgetForm()
})

const addExpenseButtonQuery = document.querySelector('#add-expense-button');
addExpenseButtonQuery.addEventListener('click', ()=>{
    resetExpenseForm();
})

const submitNewBudgetButtonQuery = document.querySelector('#addBudgetButton');

submitNewBudgetButtonQuery.addEventListener('click', ()=>{
    const amountQuery = document.querySelector('#new-budget-amount');
    const amount = amountQuery.value;

    if(!amount.trim()){
        Swal.fire('Error', 'Please enter budget amount!', 'error')
        return
    }

    myBudget.addNewBudget(parseInt(amount));
    renderCardAmount()
    renderExpenseList()
    resetBudgetForm();
    $('#addBudget').modal('hide')
    Swal.fire('Success', 'New budget added successfully!', 'success')
})

const submitNewExpenseButtonQuery = document.querySelector('#addExpenseButton');

submitNewExpenseButtonQuery.addEventListener('click', ()=>{
    const expenseQuery = document.querySelector('#new-expense');
    const amountQuery = document.querySelector('#new-expense-amount');
    const expense = expenseQuery.value;
    const amount = amountQuery.value;

    if(!expense.trim()){
        Swal.fire('Error', 'Expense is required!', 'error')        
        return
    }else if(!amount.trim()){
        Swal.fire('Error', 'Amount is required!', 'error')        
        return
    }

    const {balance} = getAmounts();

    if(balance == 0){
        Swal.fire('Error', 'You cant add more expense!', 'error')        
        return
    }else if(balance< amount){
        Swal.fire('Error', 'Your balance is low add lower amount expense!', 'error')        
        return
    }

    myBudget.addExpense(expense, parseInt(amount));
    renderCardAmount()
    renderExpenseList()
    resetExpenseForm()
    $('#addExpense').modal('hide')
    Swal.fire('Success', 'New Expense added successfully!', 'success')        
})

const renderExpenseList = () =>{
    const expenses = myBudget.getExpenseList()
    view.renderExpenseList(expenses)

    if(expenses.length){
        var deleteElements = document.getElementsByClassName("delete-expense");
        var editElements = document.getElementsByClassName("edit-expense");
        
        Array.from(deleteElements).forEach(function(element) {
            element.addEventListener('click',  function(){
                deleteExpense(this.id);
            });
        });

        Array.from(editElements).forEach(function(element) {
            element.addEventListener('click',  function(){
                editExpenseModel(this.id);
            });
        });
    }
}

const renderCardAmount = () => {
    view.renderCardAmount(getAmounts())
}

renderCardAmount();
renderExpenseList();
getCategoryList();
getRandomJokes();
    
setInterval(function(){ 
    getRandomJokes();
 }, 10000);

