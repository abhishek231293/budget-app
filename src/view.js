import Budget from './budget'

class View{

    renderExpenseList(expenses){
        
        let content = '';
        
        expenses.forEach((expense, key)=>{
            content += `<tr>
                            <th scope="row">${key+1}</th>
                            <td>${expense.expense}</td>
                            <td>$${expense.amount}</td>
                            <td>
                                <a title="Edit Expense" class='edit-expense' id='${expense.id}'> Edit </a>
                                <a title="Delete Expense" class='delete-expense' id='${expense.id}'> Delete </a>
                            </td>
                        </tr>`
        })
    
        if(!content){
            content = `<tr> <th colspan="4"> <span class="no-data">No Expense Available</span> </th> </tr>`
            const listQuery = document.querySelector('#expense-list-body')
            listQuery.innerHTML = content;
        }else{
            const listQuery = document.querySelector('#expense-list-body')
            listQuery.innerHTML = content;
            
        }
    }

    renderCardAmount({ balance, expense, budget }) {
        
        const budgetQuery = document.querySelector('#budget')
        budgetQuery.innerHTML = `$${budget}`;
    
        const expenseQuery = document.querySelector('#expense')
        expenseQuery.innerHTML = `$${expense}`;
    
        const balanceQuery = document.querySelector('#balance')
        balanceQuery.innerHTML = `$${balance}`;
    
    }

}

module.exports = View